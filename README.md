###############################################
##  TPMA
##  Abdourahamane BOinaidi et Aly-Nayef Basma
## 2019 - 2020
###############################################

# Introduction
Dans ce tp on devait créer une application trello en J2EE, en utilisant la technologie JSF.

# Avancement du tp
 - Authentification / Inscription
 - Créer un backlog
 - Les autres options c'est dans le code, car la façon dont nous voulions faire l'application, ce n'étais pas compatible avec JSF.

# Configuration de la bdd
Veuillez retrouvez la config de la bdd, dans la classe `BacklogEJB` dans le package `ejb`.