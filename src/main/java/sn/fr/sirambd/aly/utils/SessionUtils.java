package sn.fr.sirambd.aly.utils;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionUtils {

    public static ExternalContext getContext() {
        return FacesContext.getCurrentInstance()
                .getExternalContext();
    }

    public static HttpSession getSession() {
        return (HttpSession) getContext().getSession(true);
    }

    public static HttpServletRequest getRequest() {
        return (HttpServletRequest) FacesContext.getCurrentInstance()
                .getExternalContext().getRequest();
    }

    public static Object getUser() {
        return getSession().getAttribute("user");
    }

    public static String getUserName() {
        HttpSession session = getSession();
        if (session != null)
            return session.getAttribute("username").toString();
        return null;
    }

    public static String getUserId() {
        HttpSession session = getSession();
        if (session != null)
            return session.getAttribute("userid").toString();
        else
            return null;
    }
}
