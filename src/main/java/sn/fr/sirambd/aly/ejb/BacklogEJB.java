package sn.fr.sirambd.aly.ejb;

import sn.fr.sirambd.aly.Constants;
import sn.fr.sirambd.aly.models.Backlog;

import javax.annotation.sql.DataSourceDefinition;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@DataSourceDefinition(
        name = "java:app/env/jdbc/MyDataSource",
        className = "com.mysql.jdbc.jdbc2.optional.MysqlDataSource",
        user = "root",
        password = "",
        serverName = "127.0.0.1",
        portNumber = 3306,
        databaseName = "tpma_db")

@LocalBean // Utiliser dans le client sans interface
@Stateless
@Remote
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class BacklogEJB {

    @PersistenceContext
    private EntityManager em;

    public BacklogEJB() {
    }

    public Backlog create(String name, String description) {
        Backlog backlog = new Backlog(name, description);
        em.persist(backlog);
        return backlog;
    }

    public Backlog find(int id) {
        Backlog backlog = em.find(Backlog.class, id);
        if (backlog == null)
            throw new EntityNotFoundException(String.format(Constants.ENTITY_NOT_FOUND_EXCEPTION, "Backlog", id));
        return backlog;
    }

    public void destroy(int id) {
        Backlog backlog = em.find(Backlog.class, id);
        em.remove(backlog);
    }

    public List<Backlog> getAll() {
        Query query = em.createQuery("select b from Backlog b", Backlog.class);
        List<Backlog> res = query.getResultList();
        return res;
    }
}
