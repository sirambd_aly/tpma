package sn.fr.sirambd.aly.ejb;

import sn.fr.sirambd.aly.models.User;

import javax.ejb.*;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.SQLException;

@LocalBean // Utiliser dans le client sans interface
@Stateless
@Remote
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class UserEJB {

    @PersistenceContext
    private EntityManager em;

    public UserEJB() {
    }

    public User find(int id) {
        return em.find(User.class, id);
    }

    public User findByPseudo(String pseudo) {
        try {
            Query query = em.createQuery("select u from User u where u.pseudo=:pseudo");
            query.setParameter("pseudo", pseudo);
            return (User) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }

    }

    public User create(String lastname, String firstname, String pseudo, String password) {
        try {
            if (findByPseudo(pseudo) == null) {
                User user = new User(lastname, firstname, pseudo, password);
                em.persist(user);
                return user;
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public User validate(String pseudo, String password) {
        try {
            Query query = em.createNamedQuery("findUser", User.class);
            query.setParameter("pseudo", pseudo);
            query.setParameter("password", password);
            return (User) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

}
