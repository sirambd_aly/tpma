package sn.fr.sirambd.aly.ejb;

import sn.fr.sirambd.aly.Constants;
import sn.fr.sirambd.aly.models.Backlog;
import sn.fr.sirambd.aly.models.BacklogColumn;

import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@LocalBean // Utiliser dans le client sans interface
@Stateless
@Remote
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ColumnEJB {

    @PersistenceContext
    private EntityManager em;

    public ColumnEJB() {
    }

    public BacklogColumn create(String name, int backlogID) {
        Backlog backlog = em.find(Backlog.class, backlogID);
        BacklogColumn column = new BacklogColumn(name);

        // Set next and prev
        List<BacklogColumn> columns;
        if (!(columns = getAll()).isEmpty()) {
            int lastColumnID = columns.get(columns.size() - 1).getId();
            BacklogColumn bColumn = em.find(BacklogColumn.class, lastColumnID);

            if (column.getId() != bColumn.getId()) {
                bColumn.setNext(column);
                column.setPrev(bColumn);
            }
        }

        backlog.addColumn(column);
        column.setBacklog(backlog);

        em.persist(column);
        return column;
    }

    public BacklogColumn find(int id) {
        BacklogColumn column = em.find(BacklogColumn.class, id);
        if (column == null)
            throw new EntityNotFoundException(String.format(Constants.ENTITY_NOT_FOUND_EXCEPTION, "Columns", id));
        return column;
    }

    public void destroy(int id) {
        BacklogColumn column = em.find(BacklogColumn.class, id);
        em.remove(column);
    }

    public List<BacklogColumn> getAll() {
        Query query = em.createQuery("select b from BacklogColumn b", BacklogColumn.class);
        return (List<BacklogColumn>) query.getResultList();
    }
}
