package sn.fr.sirambd.aly.ejb;

import sn.fr.sirambd.aly.Constants;
import sn.fr.sirambd.aly.models.BacklogColumn;
import sn.fr.sirambd.aly.models.Entry;

import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@LocalBean // Utiliser dans le client sans interface
@Stateless
@Remote
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class EntryEJB {

    @PersistenceContext
    private EntityManager em;

    public EntryEJB() {
    }

    public Entry create(String name, int priority, int estimation, String description, int columnID) {
        Entry entry = new Entry(name, priority, estimation, description);
        BacklogColumn column = em.find(BacklogColumn.class, columnID);

        column.addEntry(entry);
        entry.setColumn(column);
        em.persist(entry);
        return entry;
    }

    public Entry find(int id) {
        Entry entry = em.find(Entry.class, id);
        if (entry == null)
            throw new EntityNotFoundException(String.format(Constants.ENTITY_NOT_FOUND_EXCEPTION, "Entry", id));
        return entry;
    }

    public void destroy(int id) {
        Entry entry = em.find(Entry.class, id);
        em.remove(entry);
    }

    public List<Entry> getAll() {
        Query query = em.createQuery("select b from Entry b", Entry.class);
        List<Entry> res = query.getResultList();
        return res;
    }
}
