package sn.fr.sirambd.aly.ejb;

import sn.fr.sirambd.aly.Constants;
import sn.fr.sirambd.aly.models.Comment;

import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@LocalBean // Utiliser dans le client sans interface
@Stateless
@Remote
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class CommentEJB {

    @PersistenceContext
    private EntityManager em;

    public CommentEJB() {
    }

    public Comment create(String value) {
        Comment comment = new Comment(value);
        em.persist(comment);
        return comment;
    }

    public Comment find(int id) {
        Comment comment = em.find(Comment.class, id);
        if (comment == null)
            throw new EntityNotFoundException(String.format(Constants.ENTITY_NOT_FOUND_EXCEPTION, "Comment", id));
        return comment;
    }

    public void destroy(int id) {
        Comment comment = em.find(Comment.class, id);
        em.remove(comment);
    }

    public List<Comment> getAll() {
        Query query = em.createQuery("select b from Comment b", Comment.class);
        List<Comment> res = query.getResultList();
        return res;
    }
}
