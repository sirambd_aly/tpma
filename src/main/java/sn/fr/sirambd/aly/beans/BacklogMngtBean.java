package sn.fr.sirambd.aly.beans;

import sn.fr.sirambd.aly.ejb.BacklogEJB;
import sn.fr.sirambd.aly.models.Backlog;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Named
@RequestScoped
public class BacklogMngtBean implements Serializable {

    @EJB
    private BacklogEJB backlogEJB;

    private Backlog backlog;
    private String name, description;

    public BacklogMngtBean() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Backlog getBacklog() {
        return backlog;
    }

    public void setBacklog(Backlog backlog) {
        this.backlog = backlog;
    }

    public void createBacklog() {
        backlogEJB.create(name, description);
    }

    public List<Backlog> getAll() {
        return backlogEJB.getAll();
    }

    public void showBacklog() {
        Map<String, String> params =
                FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        int backlogId = Integer.parseInt(params.get("backlog_id"));
        backlog = backlogEJB.find(backlogId);
    }
}