package sn.fr.sirambd.aly.beans;

import sn.fr.sirambd.aly.ejb.UserEJB;
import sn.fr.sirambd.aly.models.User;
import sn.fr.sirambd.aly.utils.SessionUtils;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import java.util.Objects;

@Named
@RequestScoped
public class UserMngtBean {

    @EJB
    private UserEJB userEJB;

    private String lastname, firstname, pseudo, password;
    private User user;

    public UserMngtBean() {
        user = null;
    }

    public UserEJB getUserEJB() {
        return userEJB;
    }

    public void setUserEJB(UserEJB userEJB) {
        this.userEJB = userEJB;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getUser() {
        if (user == null && SessionUtils.getUser() != null) {
            user = (User) SessionUtils.getUser();
        }
        return user;
    }

    public boolean isAuth() {
        return SessionUtils.getUser() != null;
    }

    //validate login
    public String validateUsernamePassword() {
        user = userEJB.validate(pseudo, password);
        if (user != null) {
            HttpSession session = SessionUtils.getSession();
            session.setAttribute("username", String.format("%s %s", firstname, lastname));
            session.setAttribute("userid", user.getId());
            session.setAttribute("user", user);
            return "/home";
        } else {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Incorrect Username and Passowrd",
                            "Please enter correct username and Password"));
            return "/accueil";
        }
    }

    public void createUser() {
        User user = userEJB.create(lastname, firstname, pseudo, password);
        if (user == null) {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Une erreur a été rencontré lors de l'inscription.",
                            "Vérifiez que vous avez bien remplis les champs"));
        }
    }

    //logout event, invalidate session
    public String logout() {
        HttpSession session = SessionUtils.getSession();
        session.invalidate();
        return "login";
    }

    public void validatePseudo(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        User user = userEJB.findByPseudo((String) value);
        if (user != null) {
            FacesMessage msg =
                    new FacesMessage("Pseudo Exists",
                            "Ce pseudo est déjà utilisé");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }
    }
}