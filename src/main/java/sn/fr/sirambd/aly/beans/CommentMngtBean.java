package sn.fr.sirambd.aly.beans;

import sn.fr.sirambd.aly.ejb.CommentEJB;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@RequestScoped
public class CommentMngtBean implements Serializable {

    @EJB
    private CommentEJB commentEJB;

    private String value;

    public CommentMngtBean() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
