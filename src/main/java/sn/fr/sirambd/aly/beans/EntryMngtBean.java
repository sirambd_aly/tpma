package sn.fr.sirambd.aly.beans;

import sn.fr.sirambd.aly.ejb.BacklogEJB;
import sn.fr.sirambd.aly.ejb.ColumnEJB;
import sn.fr.sirambd.aly.ejb.EntryEJB;
import sn.fr.sirambd.aly.models.Backlog;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Map;

@Named
@RequestScoped
public class EntryMngtBean implements Serializable {

    @EJB
    private EntryEJB entryEJB;
    @EJB
    private ColumnEJB columnEJB;
    @EJB
    private BacklogEJB backlogEJB;

    private String name, description;
    private int priority = 0, estimation, columnID = 0;

    public EntryMngtBean() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getEstimation() {
        return estimation;
    }

    public void setEstimation(int estimation) {
        this.estimation = estimation;
    }

    public int getColumnID() {
        return columnID;
    }

    public void setColumnID(int columnID) {
        this.columnID = columnID;
    }

    public String createEntry() {
        Map<String, String> params =
                FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        int columnId = Integer.parseInt(params.get("column_id"));
        int backlogId = Integer.parseInt(params.get("backlog_id"));

        FacesContext facesContext = FacesContext.getCurrentInstance();
        BacklogMngtBean backlogMngtBean
                = facesContext.getApplication().evaluateExpressionGet(facesContext, "#{backlogMngtBean}", BacklogMngtBean.class);

        int mPriority = entryEJB.getAll().size() + 1;
        entryEJB.create(name, mPriority, estimation, description, columnId);

        Backlog backlog = backlogEJB.find(backlogId);
        backlogMngtBean.setBacklog(backlog);
        return "createEntry";
    }
}
