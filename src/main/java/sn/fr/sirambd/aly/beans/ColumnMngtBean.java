package sn.fr.sirambd.aly.beans;

import sn.fr.sirambd.aly.ejb.BacklogEJB;
import sn.fr.sirambd.aly.ejb.ColumnEJB;
import sn.fr.sirambd.aly.models.Backlog;
import sn.fr.sirambd.aly.models.BacklogColumn;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Named
@RequestScoped
public class ColumnMngtBean implements Serializable {

    @EJB
    private ColumnEJB columnEJB;
    @EJB
    private BacklogEJB backlogEJB;

    private String name;

    public ColumnMngtBean() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void createColumn() {
        Map<String, String> params =
                FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        int backlogId = Integer.parseInt(params.get("backlog_id"));

        FacesContext facesContext = FacesContext.getCurrentInstance();
        BacklogMngtBean backlogMngtBean
                = facesContext.getApplication().evaluateExpressionGet(facesContext, "#{backlogMngtBean}", BacklogMngtBean.class);

        columnEJB.create(name, backlogId);

        Backlog backlog = backlogEJB.find(backlogId);
        backlogMngtBean.setBacklog(backlog);
    }

    public List<BacklogColumn> getAll() {
        return columnEJB.getAll();
    }
}
