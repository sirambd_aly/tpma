package sn.fr.sirambd.aly.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

@NamedQueries({
        @NamedQuery(name = "findUser", query = "select u from User u where u.pseudo=:pseudo and u.password=:password")
})
@Entity
@Table(name = "users")
public class User implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "user_id")
    private int id;

    @Column(name = "user_lastname", nullable = false)
    private String lastname;

    @Column(name = "user_firstname", nullable = false)
    private String firstname;

    @Column(name = "user_pseudo", nullable = false, unique = true)
    private String pseudo;

    @Column(name = "user_password", nullable = false)
    private String password;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Comment> comments = new LinkedList<>();

    public User(String lastname, String firstname, String pseudo, String password) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.pseudo = pseudo;
        this.password = password;
    }

    public User() {
    }

    public void addComment(Comment comment) {
        comments.add(comment);
    }

    public int getId() {
        return id;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}
