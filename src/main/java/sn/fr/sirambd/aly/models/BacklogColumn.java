package sn.fr.sirambd.aly.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "columns")
public class BacklogColumn implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "column_id")
    private int id;

    @Column(name = "column_name", nullable = false)
    private String name;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "column_next", referencedColumnName = "column_id")
    private BacklogColumn next;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "column_prev", referencedColumnName = "column_id")
    private BacklogColumn prev;

    @OneToMany(mappedBy = "column", cascade = CascadeType.ALL)
    private List<Entry> entries = new LinkedList<>();

    @ManyToOne
    @JoinColumn(name = "backlog_id", referencedColumnName = "backlog_id", nullable = false)
    private Backlog backlog;


    public BacklogColumn(String name) {
        this.name = name;
    }

    public BacklogColumn() {
    }

    public void addEntry(Entry entry) {
        entries.add(entry);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BacklogColumn getNext() {
        return next;
    }

    public void setNext(BacklogColumn next) {
        this.next = next;
    }

    public BacklogColumn getPrev() {
        return prev;
    }

    public void setPrev(BacklogColumn prev) {
        this.prev = prev;
    }

    public List<Entry> getEntries() {
        return entries;
    }

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }

    public Backlog getBacklog() {
        return backlog;
    }

    public void setBacklog(Backlog backlog) {
        this.backlog = backlog;
    }
}
