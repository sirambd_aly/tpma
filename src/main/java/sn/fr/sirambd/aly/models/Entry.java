package sn.fr.sirambd.aly.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "entries")
public class Entry implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "entry_id")
    private int id;

    @Column(name = "entry_name", nullable = false)
    private String name;

    @Column(name = "entry_priority", nullable = false)
    private int priority;

    @Column(name = "entry_estimation", nullable = false)
    private int estimation;

    @Column(name = "entry_description", nullable = false)
    private String description;

//    @Column(name = "entry_created")
//    @Temporal(TemporalType.DATE)
//    private Date created;
//
//    @Column(name = "entry_updated")
//    @Temporal(TemporalType.DATE)
//    private Date updated;

    @OneToMany(mappedBy = "entry", cascade = CascadeType.ALL)
    private List<Comment> comments = new LinkedList<>();

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "column_id", referencedColumnName = "column_id")
    private BacklogColumn column;

    public Entry(String name, int priority, int estimation, String description) {
        this.name = name;
        this.priority = priority;
        this.estimation = estimation;
        this.description = description;
    }

    public Entry() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getEstimation() {
        return estimation;
    }

    public void setEstimation(int estimation) {
        this.estimation = estimation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BacklogColumn getColumn() {
        return column;
    }

    public void setColumn(BacklogColumn column) {
        this.column = column;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}
