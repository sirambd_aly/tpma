package sn.fr.sirambd.aly.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "comments")
public class Comment implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "comment_id")
    private int id;

    @Column(name = "comment_value", nullable = false)
    private String value;

//    @Column(name = "comment_created", nullable = false)
//    @Temporal(TemporalType.DATE)
//    private Date created;
//
//    @Column(name = "comment_updated", nullable = false)
//    @Temporal(TemporalType.DATE)
//    private Date updated;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "entry_id", referencedColumnName = "entry_id")
    private Entry entry;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private User user;

    public Comment(String value) {
        this.value = value;
//        this.created = new Date();
//        this.updated = new Date();
    }

    public Comment() {
//        this.created = new Date();
//        this.updated = new Date();
    }

    public int getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Entry getEntry() {
        return entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
