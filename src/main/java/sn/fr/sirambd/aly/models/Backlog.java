package sn.fr.sirambd.aly.models;


import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "backlogs")
public class Backlog implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "backlog_id")
    private int id;

    @Column(name = "backlog_name", nullable = false)
    private String name;

    @Column(name = "backlog_description", nullable = false)
    private String description;

//    @Basic
//    @Column(name = "backlog_created", nullable = false)
//    @Temporal(TemporalType.DATE)
//    private Date created;

    @OneToMany(mappedBy = "backlog", cascade = CascadeType.PERSIST)
    private List<BacklogColumn> backlogColumns = new LinkedList<>();

    public Backlog(String name, String description) {
        this.name = name;
        this.description = description;
//        created = new Date();
    }

    public Backlog() {
//        created = new Date();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<BacklogColumn> getBacklogColumns() {
        return backlogColumns;
    }

    public void setBacklogColumns(List<BacklogColumn> backlogColumns) {
        this.backlogColumns = backlogColumns;
    }

    public void addColumn(BacklogColumn column) {
        backlogColumns.add(column);
    }
}
